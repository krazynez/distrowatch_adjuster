import os

def get_path():
    path = os.environ['PATH'].split(os.pathsep)
    new_path = ''
    for i, value in enumerate(path):
        new_path += value+':'
        if i + 1 == len(path):
            new_path += value
    return new_path
