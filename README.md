# Lets make Arch \#1 on Distrowatch together!


## **REQUIREMENTS**
[geckodriver](https://github.com/mozilla/geckodriver/releases) for Firefox in your PATH


## Setup

   `pip3 install -r requirements.txt`

## Running

   `python3 run.py`


### enjoy :-)
