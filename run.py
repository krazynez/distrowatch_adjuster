#!/usr/bin/env python3
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from crontab import CronTab
import os, getpass, time, get_path
from pathlib import Path


def main():


    try:
        firefoxOptions = Options()
        firefoxOptions.headless = True
        driver = webdriver.Firefox(options=firefoxOptions)
        driver.get("https://distrowatch.com/table.php?distribution=arch")
        time.sleep(10)
        driver.close()
        cron_check = Path(f'{os.getcwd()}/cron_created')
        if cron_check.is_file():
            quit()
        else:
            pass
        
        cron = input("Would you like to setup a cron job to run daily? y/n: ")
    
        if cron == 'y':
            user_name = getpass.getuser()
            cron = CronTab(user=True)
            cmd = f'export PATH={get_path.get_path()} ; cd '+os.getcwd() +' ; python3 '+os.path.basename(__file__) 

            job = cron.new(command=cmd, comment='runs the distrowatch Arch multiplier')
            job.hour.every(24)
            #job.minute.every(1)
            cron.write()
            print('created cron job')
            Path('./cron_created').touch()
        quit()
    except Exception as e:
        print('\nSomthing went wrong. Maybe you need the gecko driver installed in your path?\n', e)
        quit()

if __name__ == '__main__':
    main()
